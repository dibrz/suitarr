#!/usr/bin/with-contenv bash

source /etc/env

if [[ $VERSION != "image" ]]; then

    rm -rf $APP_DIR/*

    if [[ $APP == "radarr" ]]; then
        case $VERSION in
            stable)     url=$(curl -fsSL "https://api.github.com/repos/radarr/radarr/releases" | jq -r .[0].assets[].browser_download_url | grep -o ".*Radarr.*.linux.tar.gz")
                        ;;
            unstable)   jobid=$(curl -fsSL "https://ci.appveyor.com/api/projects/galli-leo/radarr-usby1/branch/develop" | jq -r '.build.jobs[0].jobId')
                        filename=$(curl -fsSL "https://ci.appveyor.com/api/buildjobs/${jobid}/artifacts" | jq -r '.[].fileName' | grep -o ".*Radarr.*.linux.tar.gz")
                        url="https://ci.appveyor.com/api/buildjobs/${jobid}/artifacts/${filename}"
                        ;;
            metis)      jobid=$(curl -fsSL "https://ci.appveyor.com/api/projects/galli-leo/radarr-usby1/branch/feature/custom-formats" | jq -r '.build.jobs[0].jobId')
                        filename=$(curl -fsSL "https://ci.appveyor.com/api/buildjobs/${jobid}/artifacts" | jq -r '.[].fileName' | grep -o ".*Radarr.*.linux.tar.gz")
                        url="https://ci.appveyor.com/api/buildjobs/${jobid}/artifacts/${filename}"
                        ;;
            *)          url="$VERSION"
                        ;;
        esac
        curl -fsSL $url | tar xzf - -C $APP_DIR --strip-components=1
    fi

    if [[ $APP == "sonarr" ]]; then
        case $VERSION in
            stable)     url="https://download.sonarr.tv/v2/master/mono/NzbDrone.master.tar.gz"
                        ;;
            unstable)   url="https://download.sonarr.tv/v2/develop/mono/NzbDrone.develop.tar.gz"
                        ;;
            *)          url="$VERSION"
                        ;;
        esac
        curl -fsSL $url | tar xzf - -C $APP_DIR --strip-components=1
    fi

    if [[ $APP == "lidarr" ]]; then
        case $VERSION in
            stable)     url=$(curl -fsSL "https://api.github.com/repos/lidarr/lidarr/releases" | jq -r .[0].assets[].browser_download_url | grep -o ".*Lidarr.*.linux.tar.gz")
                        ;;
            unstable)   jobid=$(curl -fsSL "https://ci.appveyor.com/api/projects/lidarr/lidarr/branch/develop" | jq -r '.build.jobs[0].jobId')
                        filename=$(curl -fsSL "https://ci.appveyor.com/api/buildjobs/${jobid}/artifacts" | jq -r '.[].fileName' | grep -o ".*Lidarr.*.linux.tar.gz")
                        url="https://ci.appveyor.com/api/buildjobs/${jobid}/artifacts/${filename}"
                        ;;
            *)          url="$VERSION"
                        ;;
        esac
        curl -fsSL $url | tar xzf - -C $APP_DIR --strip-components=1
    fi

    if [[ $APP == "jackett" ]]; then
        case $VERSION in
            stable)     url=$(curl -fsSL "https://api.github.com/repos/jackett/jackett/releases/latest" | jq -r .assets[].browser_download_url | grep -o ".*Jackett.Binaries.Mono.tar.gz")
                        ;;
            unstable)   url=$(curl -fsSL "https://api.github.com/repos/jackett/jackett/releases" | jq -r .[0].assets[].browser_download_url | grep -o ".*Jackett.Binaries.Mono.tar.gz")
                        ;;
            *)          url="$VERSION"
                        ;;
        esac
        curl -fsSL $url | tar xzf - -C $APP_DIR --strip-components=1
    fi

    if [[ $APP == "nzbhydra2" ]]; then
        case $VERSION in
            stable)     url=$(curl -fsSL "https://api.github.com/repos/theotherp/nzbhydra2/releases/latest" | jq -r .assets[].browser_download_url | grep -o ".*nzbhydra2-.*-linux.zip")
                        ;;
            unstable)   url=$(curl -fsSL "https://api.github.com/repos/theotherp/nzbhydra2/releases" | jq -r .[0].assets[].browser_download_url | grep -o ".*nzbhydra2-.*-linux.zip")
                        ;;
            *)          url="$VERSION"
                        ;;
        esac
        curl -fsSL -o /tmp/app.zip $url && unzip -q /tmp/app.zip -d $APP_DIR && rm /tmp/app.zip
        chmod +x $APP_DIR/nzbhydra2
    fi

    if [[ $APP == "nzbget" ]]; then
        case $VERSION in
            stable)     url=$(curl -fsSL "https://api.github.com/repos/nzbget/nzbget/releases/latest" | jq -r .assets[].browser_download_url | grep -o ".*nzbget-.*-bin-linux.run")
                        ;;
            unstable)   url=$(curl -fsSL "https://api.github.com/repos/nzbget/nzbget/releases" | jq -r .[0].assets[].browser_download_url | grep -o ".*nzbget-.*-bin-linux.run")
                        ;;
            *)          url="$VERSION"
                        ;;
        esac
        curl -fsSL -o /tmp/app.run $url && sh /tmp/app.run --destdir $APP_DIR && rm /tmp/app.run
    fi

    if [[ $APP == "bazarr" ]]; then
        case $VERSION in
            stable)     url="https://github.com/morpheus65535/bazarr/archive/master.tar.gz"
                        ;;
            unstable)   url="https://github.com/morpheus65535/bazarr/archive/development.tar.gz"
                        ;;
            *)          url="$VERSION"
                        ;;
        esac
        curl -fsSL $url | tar xzf - -C $APP_DIR --strip-components=1
    fi

    if [[ $APP == "sabnzbd" ]]; then
        case $VERSION in
            stable)     url=$(curl -fsSL "https://api.github.com/repos/sabnzbd/sabnzbd/releases/latest" | jq -r .assets[].browser_download_url | grep -o ".*SABnzbd-.*-src.tar.gz")
                        ;;
            unstable)   url=$(curl -fsSL "https://api.github.com/repos/sabnzbd/sabnzbd/releases" | jq -r .[0].assets[].browser_download_url | grep -o ".*SABnzbd-.*-src.tar.gz")
                        ;;
            *)          url="$VERSION"
                        ;;
        esac
        curl -fsSL $url | tar xzf - -C $APP_DIR --strip-components=1
    fi

    if [[ $APP == "telly" ]]; then
        case $VERSION in
            stable)     url=$(curl -fsSL "https://api.github.com/repos/tombowditch/telly/releases/latest" | jq -r .assets[].browser_download_url | grep -o ".*telly-linux-arm64")
                        ;;
            unstable)   url=$(curl -fsSL "https://api.github.com/repos/tombowditch/telly/releases" | jq -r .[0].assets[].browser_download_url | grep -o ".*telly-linux-arm64")
                        ;;
            *)          url="$VERSION"
                        ;;
        esac
        curl -fsSL -o $APP_DIR/telly $url
        chmod +x $APP_DIR/telly
    fi

    if [[ $APP == "ombi" ]]; then
        case $VERSION in
            stable)     url=$(curl -fsSL "https://api.github.com/repos/tidusjar/ombi/releases/latest" | jq -r .assets[].browser_download_url | grep -o ".*linux.tar.gz")
                        ;;
            unstable)   url=$(curl -fsSL "https://api.github.com/repos/tidusjar/ombi/releases" | jq -r .[0].assets[].browser_download_url | grep -o ".*linux.tar.gz")
                        ;;
            *)          url="$VERSION"
                        ;;
        esac
        curl -fsSL $url | tar xzf - -C $APP_DIR
    fi

    if [[ $APP == "tautulli" ]]; then
        case $VERSION in
            stable)     url=$(curl -fsSL "https://api.github.com/repos/tautulli/tautulli/releases/latest" | jq -r .tarball_url)
                        ;;
            unstable)   url=$(curl -fsSL "https://api.github.com/repos/tautulli/tautulli/releases" | jq -r .[0].tarball_url)
                        ;;
            *)          url="$VERSION"
                        ;;
        esac
        curl -fsSL $url | tar xzf - -C $APP_DIR --strip-components=1
    fi

fi

chown -R dibrz:dibrz $APP_DIR
