## What is Suitarr?

Suitarr is a collection of docker images. By using different docker tags, you'll install different apps. Suitarr also gives you the option of installing different versions for every app.

## Starting the container

The environment variables `PUID`, `PGID`, `UMASK`, `VERSION` and `BACKUP` are all optional, the values you see below are the default values.

By default the image comes with a stable version. You can however install a different version with the environment variable `VERSION`. The value `image` does nothing, but keep the included version, all the others install a different version when starting the container.

#### [Radarr](https://github.com/Radarr/Radarr)
```
docker run --rm \
           --name radarr \
           -p 7878:7878 \
           -e PUID=1000 \
           -e PGID=1000 \
           -e UMASK=022 \
           -e VERSION=image \
           -e BACKUP=yes \
           -v /etc/localtime:/etc/localtime:ro \
           -v /<local_path>/config:/config \
           dibrz/suitarr:radarr
```

```
-e VERSION=image
-e VERSION=stable
-e VERSION=unstable
-e VERSION=metis
-e VERSION=https://github.com/Radarr/Radarr/releases/download/v0.2.0.1120/Radarr.develop.0.2.0.1120.linux.tar.gz
-e VERSION=file:///config/Radarr.develop.0.2.0.1120.linux.tar.gz
```

#### [Sonarr](https://github.com/Sonarr/Sonarr)
```
docker run --rm \
           --name sonarr \
           -p 8989:8989 \
           -e PUID=1000 \
           -e PGID=1000 \
           -e UMASK=022 \
           -e VERSION=image \
           -e BACKUP=yes \
           -v /etc/localtime:/etc/localtime:ro \
           -v /<local_path>/config:/config \
           dibrz/suitarr:sonarr
```

```
-e VERSION=image
-e VERSION=stable
-e VERSION=unstable
-e VERSION=http://download.sonarr.tv/v2/master/mono/NzbDrone.master.2.0.0.5228.mono.tar.gz
-e VERSION=file:///config/NzbDrone.master.2.0.0.5228.mono.tar.gz
```

#### [Lidarr](https://github.com/lidarr/Lidarr)
```
docker run --rm \
           --name lidarr \
           -p 8686:8686 \
           -e PUID=1000 \
           -e PGID=1000 \
           -e UMASK=022 \
           -e VERSION=image \
           -e BACKUP=yes \
           -v /etc/localtime:/etc/localtime:ro \
           -v /<local_path>/config:/config \
           dibrz/suitarr:lidarr
```

```
-e VERSION=image
-e VERSION=stable
-e VERSION=unstable
-e VERSION=https://github.com/lidarr/Lidarr/releases/download/v0.3.0.430/Lidarr.develop.0.3.0.430.linux.tar.gz
-e VERSION=file:///config/Lidarr.develop.0.3.0.430.linux.tar.gz
```

#### [Jackett](https://github.com/Jackett/Jackett)
```
docker run --rm \
           --name jackett \
           -p 9117:9117 \
           -e PUID=1000 \
           -e PGID=1000 \
           -e UMASK=022 \
           -e VERSION=image \
           -e BACKUP=yes \
           -v /etc/localtime:/etc/localtime:ro \
           -v /<local_path>/config:/config \
           dibrz/suitarr:jackett
```

```
-e VERSION=image
-e VERSION=stable
-e VERSION=unstable
-e VERSION=https://github.com/Jackett/Jackett/releases/download/v0.8.1162/Jackett.Binaries.Mono.tar.gz
-e VERSION=file:///config/Jackett.Binaries.Mono.tar.gz
```

#### [NZBHydra2](https://github.com/theotherp/nzbhydra2)
```
docker run --rm \
           --name nzbhydra2 \
           -p 5076:5076 \
           -e PUID=1000 \
           -e PGID=1000 \
           -e UMASK=022 \
           -e VERSION=image \
           -e BACKUP=yes \
           -v /etc/localtime:/etc/localtime:ro \
           -v /<local_path>/config:/config \
           dibrz/suitarr:nzbhydra2
```

```
-e VERSION=image
-e VERSION=stable
-e VERSION=unstable
-e VERSION=https://github.com/theotherp/nzbhydra2/releases/download/v1.5.1/nzbhydra2-1.5.1-linux.zip
-e VERSION=file:///config/nzbhydra2-1.5.1-linux.zip
```

#### [NZBGet](https://github.com/nzbget/nzbget)
```
docker run --rm \
           --name nzbget \
           -p 6789:6789 \
           -e PUID=1000 \
           -e PGID=1000 \
           -e UMASK=022 \
           -e VERSION=image \
           -e BACKUP=yes \
           -v /etc/localtime:/etc/localtime:ro \
           -v /<local_path>/config:/config \
           dibrz/suitarr:nzbget
```

```
-e VERSION=image
-e VERSION=stable
-e VERSION=unstable
-e VERSION=https://github.com/nzbget/nzbget/releases/download/v20.0/nzbget-20.0-bin-linux.run
-e VERSION=file:///config/nzbget-20.0-bin-linux.run
```

#### [Bazarr](https://github.com/morpheus65535/bazarr)
```
docker run --rm \
           --name bazarr \
           -p 6767:6767 \
           -e PUID=1000 \
           -e PGID=1000 \
           -e UMASK=022 \
           -e VERSION=image \
           -e BACKUP=yes \
           -v /etc/localtime:/etc/localtime:ro \
           -v /<local_path>/config:/config \
           dibrz/suitarr:bazarr
```

```
-e VERSION=image
-e VERSION=stable
-e VERSION=unstable
-e VERSION=https://github.com/morpheus65535/bazarr/archive/master.tar.gz
-e VERSION=file:///config/master.tar.gz
```

#### [SABnzbd](https://github.com/sabnzbd/sabnzbd)
```
docker run --rm \
           --name sabnzbd \
           -p 8080:8080 \
           -e PUID=1000 \
           -e PGID=1000 \
           -e UMASK=022 \
           -e VERSION=image \
           -e BACKUP=yes \
           -v /etc/localtime:/etc/localtime:ro \
           -v /<local_path>/config:/config \
           dibrz/suitarr:sabnzbd
```

```
-e VERSION=image
-e VERSION=stable
-e VERSION=unstable
-e VERSION=https://github.com/sabnzbd/sabnzbd/releases/download/2.3.4/SABnzbd-2.3.4-src.tar.gz
-e VERSION=file:///config/SABnzbd-2.3.4-src.tar.gz
```

#### [Telly](https://github.com/tombowditch/telly)
```
docker run --rm \
           --name telly \
           -p 6077:6077 \
           -e PUID=1000 \
           -e PGID=1000 \
           -e UMASK=022 \
           -e VERSION=image \
           -e BACKUP=yes \
           -e ARGS="-listen=localhost:6077 -base=localhost:6077 -playlist=/config/app/myiptv.m3u" \
           -v /etc/localtime:/etc/localtime:ro \
           -v /<local_path>/config:/config \
           dibrz/suitarr:telly
```

```
-e VERSION=image
-e VERSION=stable
-e VERSION=unstable
-e VERSION=https://github.com/tombowditch/telly/releases/download/v0.6.2/telly-linux-arm64
-e VERSION=file:///config/telly-linux-arm64
```

#### [Ombi](https://github.com/tidusjar/Ombi)
```
docker run --rm \
           --name ombi \
           -p 5000:5000 \
           -e PUID=1000 \
           -e PGID=1000 \
           -e UMASK=022 \
           -e VERSION=image \
           -e BACKUP=yes \
           -v /etc/localtime:/etc/localtime:ro \
           -v /<local_path>/config:/config \
           dibrz/suitarr:ombi
```

```
-e VERSION=image
-e VERSION=stable
-e VERSION=unstable
-e VERSION=https://github.com/tidusjar/Ombi/releases/download/v3.0.3477/linux.tar.gz
-e VERSION=file:///config/linux.tar.gz
```

#### [Tautulli](https://github.com/Tautulli/Tautulli)
```
docker run --rm \
           --name tautulli \
           -p 8181:8181 \
           -e PUID=1000 \
           -e PGID=1000 \
           -e UMASK=022 \
           -e VERSION=image \
           -e BACKUP=yes \
           -v /etc/localtime:/etc/localtime:ro \
           -v /<local_path>/config:/config \
           dibrz/suitarr:tautulli
```

```
-e VERSION=image
-e VERSION=stable
-e VERSION=unstable
-e VERSION=https://github.com/Tautulli/Tautulli/archive/v2.1.14.tar.gz
-e VERSION=file:///config/v2.1.14.tar.gz
```

## Docker Registry

You can also use the GitLab docker registry as an alternative, by using `registry.gitlab.com/dibrz/suitarr:...` instead of `dibrz/suitarr:...`.

## Backing up the configuration

By default on every docker container shutdown a backup is created from the configuration files. To disable this, use `-e BACKUP=no`.

## Additional app arguments

You can use the following environment variable to pass on additional arguments to your app.

```
-e ARGS="--ProxyConnection localhost:8118"
```

## Using a rclone mount

Mounting a remote filesystem using `rclone` can be done with the option `-e RCLONE="..."`, see below for a more detailed example. Use `docker exec -it --user dibrz CONTAINERNAME rclone config` to configure your remote when the container is running. Configuration files for `rclone` are stored in `/config/.config/rclone`.

```
-e RCLONE="remote1:path/to/files,/localmount1|remote2:path/to/files,/localmount2"
```

In most cases you will need some or all of the following flags added to your command to get the required docker privileges.

```
 --security-opt apparmor:unconfined --cap-add SYS_ADMIN --device /dev/fuse
```

## Using a rar2fs mount

Mounting a filesystem using `rar2fs` can be done with the option `-e RAR2FS="..."`, see below for a more detailed example. The new mount will be read-only. Using a `rar2fs` mount makes the use of an unrar script obsolete. You can mount a `rar2fs` mount on top of an `rclone` mount, `rclone` mounts are mounted first.

```
-e RAR2FS="/folder1-rar,/folder1-unrar|/folder2-rar,/folder2-unrar"
```

In most cases you will need some or all of the following flags added to your command to get the required docker privileges.

```
 --security-opt apparmor:unconfined --cap-add SYS_ADMIN --device /dev/fuse
```
