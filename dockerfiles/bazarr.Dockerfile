ARG BRANCH
FROM dibrz/suitarr:base${BRANCH}

ENV APP="bazarr"
EXPOSE 6767
HEALTHCHECK --interval=60s CMD curl -fsSL http://localhost:6767 || exit 1

# install packages
RUN apt-get update && \
    apt-get install -y --no-install-recommends --no-install-suggests \
        python-pip python-setuptools python-wheel libjpeg8-dev git && \
# clean up
    apt-get autoremove -y && \
    apt-get clean && \
    rm -rf /tmp/* /var/lib/apt/lists/* /var/tmp/*

# install app
RUN url="https://github.com/morpheus65535/bazarr/archive/2db22f027140027c51c13b73e5fdc36f4d5da47a.tar.gz" && \
    curl -fsSL $url | tar xzf - -C $APP_DIR --strip-components=1

COPY root/ /
