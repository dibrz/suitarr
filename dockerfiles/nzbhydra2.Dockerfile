ARG BRANCH
FROM dibrz/suitarr:java${BRANCH}

ENV APP="nzbhydra2"
EXPOSE 5076
HEALTHCHECK --interval=60s CMD curl -fsSL http://localhost:5076 || exit 1

# install app
RUN url="https://github.com/theotherp/nzbhydra2/releases/download/v1.5.2/nzbhydra2-1.5.2-linux.zip" && \
    curl -fsSL -o /tmp/app.zip $url && unzip -q /tmp/app.zip -d $APP_DIR && rm /tmp/app.zip && \
    chmod +x $APP_DIR/nzbhydra2

COPY root/ /
