ARG BRANCH
FROM dibrz/suitarr:mono${BRANCH}

ENV APP="jackett"
EXPOSE 9117
HEALTHCHECK --interval=60s CMD curl -fsSL http://localhost:9117 || exit 1

# install app
RUN url="https://github.com/Jackett/Jackett/releases/download/v0.9.41/Jackett.Binaries.Mono.tar.gz" && \
    curl -fsSL $url | tar xzf - -C $APP_DIR --strip-components=1

COPY root/ /
