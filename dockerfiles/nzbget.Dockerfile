ARG BRANCH
FROM dibrz/suitarr:base${BRANCH}

ENV APP="nzbget"
EXPOSE 6789
HEALTHCHECK --interval=60s CMD curl -fsSL http://localhost:6789 || exit 1

# install app
RUN url="https://github.com/nzbget/nzbget/releases/download/v20.0/nzbget-20.0-bin-linux.run" && \
    curl -fsSL -o /tmp/app.run $url && sh /tmp/app.run --destdir $APP_DIR && rm /tmp/app.run

COPY root/ /
