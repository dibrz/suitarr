ARG BRANCH
FROM dibrz/suitarr:mono${BRANCH}

ENV APP="sonarr"
EXPOSE 8989
HEALTHCHECK --interval=60s CMD curl -fsSL http://localhost:8989 || exit 1

# install app
RUN url="https://download.sonarr.tv/v2/master/mono/NzbDrone.master.2.0.0.5228.mono.tar.gz" && \
    curl -fsSL $url | tar xzf - -C $APP_DIR --strip-components=1

COPY root/ /
