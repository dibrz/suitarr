ARG BRANCH
FROM dibrz/suitarr:base${BRANCH}

ENV APP="ombi"
EXPOSE 5000
HEALTHCHECK --interval=60s CMD curl -fsSL http://localhost:5000 || exit 1

# install packages
RUN apt-get update && \
    apt-get install -y --no-install-recommends --no-install-suggests \
        libssl1.0.0 libicu60 libunwind8 && \
# clean up
    apt-get autoremove -y && \
    apt-get clean && \
    rm -rf /tmp/* /var/lib/apt/lists/* /var/tmp/*

# install app
RUN url="https://github.com/tidusjar/Ombi/releases/download/v3.0.3477/linux.tar.gz" && \
    curl -fsSL $url | tar xzf - -C $APP_DIR

COPY root/ /
