ARG BRANCH
FROM dibrz/suitarr:base${BRANCH}

ENV APP="telly"
EXPOSE 6077
HEALTHCHECK --interval=60s CMD curl -fsSL http://localhost:6077 || exit 1

# install app
RUN url="https://github.com/tombowditch/telly/releases/download/v0.6.2/telly-linux-arm64" && \
    curl -fsSL -o $APP_DIR/telly $url && \
    chmod +x $APP_DIR/telly

COPY root/ /
