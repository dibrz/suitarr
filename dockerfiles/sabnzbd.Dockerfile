ARG BRANCH
FROM dibrz/suitarr:base${BRANCH}

ENV APP="sabnzbd"
EXPOSE 8080
HEALTHCHECK --interval=60s CMD curl -fsSL http://localhost:8080 || exit 1

# install packages
RUN apt-get update && \
    apt-get install -y --no-install-recommends --no-install-suggests \
        software-properties-common && \
    add-apt-repository ppa:jcfp/sab-addons && \
    apt-get update && \
    apt-get install -y --no-install-recommends --no-install-suggests \
        python-cheetah python-sabyenc python-cryptography par2-tbb unrar p7zip-full && \
# clean up
    apt-get purge -y software-properties-common && \
    apt-get autoremove -y && \
    apt-get clean && \
    rm -rf /tmp/* /var/lib/apt/lists/* /var/tmp/*

# install app
RUN url="https://github.com/sabnzbd/sabnzbd/releases/download/2.3.4/SABnzbd-2.3.4-src.tar.gz" && \
    curl -fsSL $url | tar xzf - -C $APP_DIR --strip-components=1

COPY root/ /
