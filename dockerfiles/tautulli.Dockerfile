ARG BRANCH
FROM dibrz/suitarr:base${BRANCH}

ENV APP="tautulli"
EXPOSE 8181
HEALTHCHECK --interval=60s CMD curl -fsSL http://localhost:8181 || exit 1

# install packages
RUN apt-get update && \
    apt-get install -y --no-install-recommends --no-install-suggests \
        python-setuptools python-pycryptodome && \
# clean up
    apt-get autoremove -y && \
    apt-get clean && \
    rm -rf /tmp/* /var/lib/apt/lists/* /var/tmp/*

# install app
RUN url="https://github.com/Tautulli/Tautulli/archive/v2.1.18.tar.gz" && \
    curl -fsSL $url | tar xzf - -C $APP_DIR --strip-components=1

COPY root/ /
