ARG BRANCH
FROM dibrz/suitarr:mono${BRANCH}

ENV APP="radarr"
EXPOSE 7878
HEALTHCHECK --interval=60s CMD curl -fsSL http://localhost:7878 || exit 1

# install app
RUN url="https://github.com/Radarr/Radarr/releases/download/v0.2.0.1120/Radarr.develop.0.2.0.1120.linux.tar.gz" && \
    curl -fsSL $url | tar xzf - -C $APP_DIR --strip-components=1

COPY root/ /
